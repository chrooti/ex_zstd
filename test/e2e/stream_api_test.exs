defmodule E2E.StreamApiTest do
  use ExUnit.Case

  alias ExZstd.CCtx
  alias ExZstd.DCtx
  alias Fixtures
  alias Utils

  test "compress + decompress" do
    chunks = [Fixtures.data(), Fixtures.data2()]

    expected = Enum.join(chunks)

    {missing_bytes, actual} =
      chunks
      |> Utils.stream_compress_chunks()
      |> then(&DCtx.decompress_stream!(DCtx.new(), &1))

    assert missing_bytes == 0
    assert expected == actual
  end

  test "compress + decompress, flush" do
    cctx = CCtx.new()
    first_chunk = CCtx.compress_stream!(cctx, Fixtures.data(), CCtx.stream_flush())
    second_chunk = CCtx.compress_stream!(cctx, Fixtures.data2(), CCtx.stream_end())

    dctx = DCtx.new()
    {missing_bytes, decompressed_chunk} = DCtx.decompress_stream!(dctx, first_chunk)

    assert missing_bytes > 0
    assert decompressed_chunk == Fixtures.data()

    {missing_bytes, decompressed_chunk} = DCtx.decompress_stream!(dctx, second_chunk)

    assert missing_bytes == 0
    assert decompressed_chunk == Fixtures.data2()
  end

  test "compress + decompress, continue" do
    cctx = CCtx.new()

    compressed_data =
      CCtx.compress_stream!(cctx, Fixtures.data(), CCtx.stream_continue()) <>
        CCtx.compress_stream!(cctx, Fixtures.data2(), CCtx.stream_end())

    <<first_chunk::size(2)-binary, second_chunk::binary>> = compressed_data

    dctx = DCtx.new()
    {missing_bytes, first_decompressed_chunk} = DCtx.decompress_stream!(dctx, first_chunk)

    assert missing_bytes > 0

    {missing_bytes, second_decompressed_chunk} = DCtx.decompress_stream!(dctx, second_chunk)

    assert missing_bytes == 0

    assert first_decompressed_chunk <> second_decompressed_chunk ==
             Fixtures.data() <> Fixtures.data2()
  end
end
