defmodule E2E.SimpleApiTest do
  use ExUnit.Case

  alias ExZstd.CCtx
  alias ExZstd.DCtx
  alias Fixtures

  test "compress + decompress" do
    expected = Fixtures.data()

    actual =
      Fixtures.data()
      |> ExZstd.compress!()
      |> ExZstd.decompress!()

    assert expected == actual
  end

  test "context compress + decompress" do
    expected = Fixtures.data()

    actual =
      CCtx.new()
      |> CCtx.compress!(Fixtures.data())
      |> then(&DCtx.decompress!(DCtx.new(), &1))

    assert expected == actual
  end

  test "context reuse" do
    cctx = CCtx.new()
    compressed_data = CCtx.compress!(cctx, Fixtures.data())
    compressed_data2 = CCtx.compress!(cctx, Fixtures.data2())

    dctx = DCtx.new()
    decompressed_data = DCtx.decompress!(dctx, compressed_data)
    decompressed_data2 = DCtx.decompress!(dctx, compressed_data2)

    assert Fixtures.data() == decompressed_data
    assert Fixtures.data2() == decompressed_data2
  end
end
