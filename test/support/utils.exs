defmodule Utils do
  alias ExZstd.CCtx

  def stream_compress_chunks(cctx \\ CCtx.new(), chunks),
    do: do_stream_compress_chunks(cctx, chunks, "")

  defp do_stream_compress_chunks(cctx, [chunk1, chunk2 | chunks], result) do
    compressed_chunk = CCtx.compress_stream!(cctx, chunk1, CCtx.stream_continue())
    do_stream_compress_chunks(cctx, [chunk2 | chunks], result <> compressed_chunk)
  end

  defp do_stream_compress_chunks(cctx, [chunk], result) do
    result <> CCtx.compress_stream!(cctx, chunk, CCtx.stream_end())
  end
end
