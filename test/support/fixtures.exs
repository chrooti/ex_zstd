defmodule Fixtures do
  @data :crypto.strong_rand_bytes(16)
  @data2 :crypto.strong_rand_bytes(16)

  def data, do: @data
  def data2, do: @data2
end
