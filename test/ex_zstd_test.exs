defmodule ExZstdTest do
  use ExUnit.Case

  alias ExZstd.Cparam
  alias ExZstd.Dparam
  alias Fixtures
  alias Utils

  describe "decompress" do
    test "content size error" do
      assert {:error, :content_size_error} == ExZstd.decompress("garbage")
    end

    test "content size error, raise" do
      assert_raise ExZstd.ContentSizeError, fn ->
        ExZstd.decompress!("garbage")
      end
    end

    test "content size unknown" do
      expected = {:error, :content_size_unknown}

      actual =
        [Fixtures.data(), Fixtures.data2()]
        |> Utils.stream_compress_chunks()
        |> ExZstd.decompress()

      assert expected == actual
    end

    test "content size unknown, raise" do
      assert_raise ExZstd.ContentSizeUnknown, fn ->
        [Fixtures.data(), Fixtures.data2()]
        |> Utils.stream_compress_chunks()
        |> ExZstd.decompress!()
      end
    end
  end

  describe "get_cparam_bounds" do
    test "happy path" do
      assert {_, _} = ExZstd.get_cparam_bounds(Cparam.compression_level())
    end

    test "invalid parameter" do
      assert_raise ExZstd.Error, "Unsupported parameter", fn ->
        ExZstd.get_cparam_bounds(999)
      end
    end
  end

  describe "get_dparam_bounds" do
    test "happy path" do
      assert {_, _} = ExZstd.get_dparam_bounds(Dparam.window_log_max())
    end

    test "invalid parameter" do
      assert_raise ExZstd.Error, "Unsupported parameter", fn ->
        ExZstd.get_dparam_bounds(999)
      end
    end
  end
end
