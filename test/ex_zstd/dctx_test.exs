defmodule ExZstd.DCtxTest do
  use ExUnit.Case

  alias ExZstd.DCtx
  alias ExZstd.Dparam
  alias Fixtures
  alias Utils

  test "content size error" do
    assert {:error, :content_size_error} == DCtx.decompress(DCtx.new(), "garbage")
  end

  test "content size error, raise" do
    assert_raise ExZstd.ContentSizeError, fn ->
      DCtx.decompress!(DCtx.new(), "garbage")
    end
  end

  test "content size unknown" do
    expected = {:error, :content_size_unknown}

    actual =
      [Fixtures.data(), Fixtures.data2()]
      |> Utils.stream_compress_chunks()
      |> then(&DCtx.decompress(DCtx.new(), &1))

    assert expected == actual
  end

  test "content size unknown, raise" do
    assert_raise ExZstd.ContentSizeUnknown, fn ->
      [Fixtures.data(), Fixtures.data2()]
      |> Utils.stream_compress_chunks()
      |> then(&DCtx.decompress!(DCtx.new(), &1))
    end
  end

  test "set and get param" do
    expected = 20

    actual =
      DCtx.new()
      |> DCtx.set_dparam(Dparam.window_log_max(), expected)
      |> DCtx.get_dparam(Dparam.window_log_max())

    assert expected == actual
  end

  test "reset" do
    expected = 20

    dctx = DCtx.new()

    actual =
      dctx
      |> DCtx.set_dparam(Dparam.window_log_max(), expected)
      |> DCtx.get_dparam(Dparam.window_log_max())

    assert expected == actual

    DCtx.reset(dctx, ExZstd.reset_session_and_parameters())

    assert 27 == DCtx.get_dparam(dctx, Dparam.window_log_max())
  end
end
