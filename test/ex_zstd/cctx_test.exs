defmodule ExZstd.CCtxTest do
  use ExUnit.Case

  alias ExZstd.CCtx
  alias ExZstd.Cparam
  alias Fixtures
  alias Utils

  # reset
  # reset

  test "set and get param" do
    expected = 20

    actual =
      CCtx.new()
      |> CCtx.set_cparam(Cparam.compression_level(), expected)
      |> CCtx.get_cparam(Cparam.compression_level())

    assert expected == actual
  end

  test "reset" do
    expected = 20

    cctx = CCtx.new()

    actual =
      cctx
      |> CCtx.set_cparam(Cparam.compression_level(), expected)
      |> CCtx.get_cparam(Cparam.compression_level())

    assert expected == actual

    CCtx.reset(cctx, ExZstd.reset_session_and_parameters())

    assert 3 == CCtx.get_cparam(cctx, Cparam.compression_level())
  end

  test "pledge_src_size" do
    expected = 99

    actual =
      CCtx.new()
      |> CCtx.set_pledged_src_size(expected)
      |> CCtx.compress_stream!(Fixtures.data(), CCtx.stream_flush())
      |> ExZstd.get_frame_content_size()

    assert actual == expected
  end
end
