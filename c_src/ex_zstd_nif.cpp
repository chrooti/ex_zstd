#include <string.h>

#include <erl_nif.h>
#include <zstd.h>

#ifndef __GNUC__
#error "ExZstd: only Clang and GCC are supported"
#endif

#define EXZSTD_INLINE static inline __attribute__((__always_inline__, __unused__))
#define EXZSTD_EXPORT __attribute__((visibility("default")))
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

namespace exzstd {

/* Types */

static ErlNifResourceType* cctx_type = nullptr;
static ErlNifResourceType* dctx_type = nullptr;
static ErlNifResourceType* singleton_type = nullptr;

static void* singleton_instance = nullptr;

static const char CONTENT_SIZE_ERROR_MSG[] = "Content size error";
static const char CONTENT_SIZE_UNKNOWN_MSG[] = "Content size unknown";

// "normal" atoms
static ERL_NIF_TERM atom_message;
static ERL_NIF_TERM atom_true;

// double underscores
static ERL_NIF_TERM atom_exception;
static ERL_NIF_TERM atom_struct;

// module names
static ERL_NIF_TERM atom_exzstd_content_size_error;
static ERL_NIF_TERM atom_exzstd_content_size_unknown;
static ERL_NIF_TERM atom_exzstd_error;

/* Generic utilities */

static ERL_NIF_TERM make_atom_len(ErlNifEnv* env, const char* name, size_t len) {
    ERL_NIF_TERM atom;
    if (enif_make_existing_atom_len(env, name, len, &atom, ERL_NIF_LATIN1)) {
        return atom;
    }
    return enif_make_atom_len(env, name, len);
}

template<size_t len>
EXZSTD_INLINE ERL_NIF_TERM make_atom(ErlNifEnv* env, const char (&name)[len]) {
    return make_atom_len(env, name, len - 1);
}

template<size_t len>
static ERL_NIF_TERM make_binary(ErlNifEnv* env, const char (&src)[len]) {
    ERL_NIF_TERM term;
    unsigned char* dst = enif_make_new_binary(env, len - 1, &term);
    memcpy(dst, src, len - 1);
    return term;
}

static ERL_NIF_TERM
raise_exception(ErlNifEnv* env, ERL_NIF_TERM exception, ERL_NIF_TERM message) {
    ERL_NIF_TERM error_term;
    ERL_NIF_TERM keys[] = {atom_exception, atom_struct, atom_message};
    ERL_NIF_TERM values[] = {atom_true, exception, message};

    enif_make_map_from_arrays(env, keys, values, ARRAY_SIZE(keys), &error_term);
    return enif_raise_exception(env, error_term);
}

static ERL_NIF_TERM raise_zstd_exception(ErlNifEnv* env, size_t code) {
    const char* const error_string = ZSTD_getErrorName(code);
    const size_t error_string_len = strlen(error_string);

    ERL_NIF_TERM const error_binary_term = enif_make_resource_binary(
        env,
        singleton_instance,
        error_string,
        error_string_len
    );

    return raise_exception(env, atom_exzstd_error, error_binary_term);
}

template<typename T>
EXZSTD_INLINE T* alloc_resource(ErlNifResourceType* type) {
    return reinterpret_cast<T*>(enif_alloc_resource(type, sizeof(T)));
}

static ERL_NIF_TERM make_resource(ErlNifEnv* env, void* ptr) {
    ERL_NIF_TERM const resource = enif_make_resource(env, ptr);
    enif_release_resource(ptr);
    return resource;
}

/* ZSTD wrappers */

static void* zstd_alloc(void* opaque, size_t size) {
    (void) opaque;

    return enif_alloc(size);
}

static void zstd_free(void* opaque, void* ptr) {
    (void) opaque;

    enif_free(ptr);
}

static ZSTD_customMem zstd_custom_mem = {
    &zstd_alloc,
    &zstd_free,
    nullptr,
};

/* Module lifecycle */

static void cctx_dtor(ErlNifEnv* env, void* data) {
    (void) env;

    auto* cctx = reinterpret_cast<ZSTD_CCtx**>(data);
    ZSTD_freeCCtx(*cctx);
}

static void dctx_dtor(ErlNifEnv* env, void* data) {
    (void) env;

    auto* dctx = reinterpret_cast<ZSTD_DCtx**>(data);
    ZSTD_freeDCtx(*dctx);
}

static ErlNifResourceType*
create_resource(ErlNifEnv* env, const char* name, void (*dtor)(ErlNifEnv*, void*)) {
    return enif_open_resource_type(
        env,
        nullptr,
        name,
        dtor,
        static_cast<ErlNifResourceFlags>(ERL_NIF_RT_CREATE | ERL_NIF_RT_TAKEOVER),
        nullptr
    );
}

static int module_setup(ErlNifEnv* env) {
    cctx_type = create_resource(env, "exzstd_cctx", cctx_dtor);
    if (cctx_type == nullptr) {
        return -1;
    }

    dctx_type = create_resource(env, "exzstd_dctx", dctx_dtor);
    if (dctx_type == nullptr) {
        return -1;
    }

    singleton_type = create_resource(env, "exzstd_singleton", nullptr);
    if (singleton_type == nullptr) {
        return -1;
    }

    singleton_instance = enif_alloc_resource(singleton_type, 0);

    atom_message = make_atom(env, "message");
    atom_true = make_atom(env, "true");

    atom_exception = make_atom(env, "__exception__");
    atom_struct = make_atom(env, "__struct__");

    atom_exzstd_content_size_error = make_atom(env, "Elixir.ExZstd.ContentSizeError");
    atom_exzstd_content_size_unknown = make_atom(env, "Elixir.ExZstd.ContentSizeUnknown");
    atom_exzstd_error = make_atom(env, "Elixir.ExZstd.Error");

    return 0;
}

static void module_teardown(ErlNifEnv* env) {
    (void) env;

    enif_free(singleton_instance);
}

static int on_load(ErlNifEnv* env, void** priv, ERL_NIF_TERM info) {
    (void) env;
    (void) priv;
    (void) info;

    return module_setup(env);
}

static int on_reload(ErlNifEnv* env, void** priv, ERL_NIF_TERM info) {
    (void) priv;
    (void) info;

    module_teardown(env);
    return module_setup(env);
}

static int on_upgrade(ErlNifEnv* env, void** priv, void** old, ERL_NIF_TERM info) {
    (void) priv;
    (void) old;
    (void) info;

    module_teardown(env);
    return module_setup(env);
}

static void on_unload(ErlNifEnv* env, void* priv) {
    (void) priv;

    module_teardown(env);
}

/* Simple API */

static ERL_NIF_TERM
get_cparam_bounds(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    unsigned int param;
    if (!enif_get_uint(env, argv[0], &param)) {
        return enif_make_badarg(env);
    }

    const ZSTD_bounds result = ZSTD_cParam_getBounds(static_cast<ZSTD_cParameter>(param));
    if (ZSTD_isError(result.error)) {
        return raise_zstd_exception(env, result.error);
    }

    return enif_make_tuple2(
        env,
        enif_make_int(env, result.lowerBound),
        enif_make_int(env, result.upperBound)
    );
}

static ERL_NIF_TERM
get_dparam_bounds(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    unsigned int param;
    if (enif_get_uint(env, argv[0], &param) == 0) {
        return enif_make_badarg(env);
    }

    const ZSTD_bounds result = ZSTD_dParam_getBounds(static_cast<ZSTD_dParameter>(param));
    if (ZSTD_isError(result.error)) {
        return raise_zstd_exception(env, result.error);
    }

    return enif_make_tuple2(
        env,
        enif_make_int(env, result.lowerBound),
        enif_make_int(env, result.upperBound)
    );
}

static ERL_NIF_TERM
content_size_unknown(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;
    (void) argv;

    return enif_make_ulong(env, ZSTD_CONTENTSIZE_UNKNOWN);
}

static ERL_NIF_TERM
get_frame_content_size(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ErlNifBinary src;
    if (!enif_inspect_binary(env, argv[0], &src)) {
        return enif_make_badarg(env);
    }

    const size_t result = ZSTD_getFrameContentSize(src.data, src.size);
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return enif_make_ulong(env, result);
}

/* Compression context API */

static ERL_NIF_TERM cctx_new(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;
    (void) argv;

    ZSTD_CCtx** const cctx = alloc_resource<ZSTD_CCtx*>(cctx_type);
    *cctx = ZSTD_createCCtx_advanced(zstd_custom_mem);

    return make_resource(env, cctx);
}

static ERL_NIF_TERM cctx_compress(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_CCtx** cctx;
    if (!enif_get_resource(env, argv[0], cctx_type, reinterpret_cast<void**>(&cctx))) {
        return enif_make_badarg(env);
    }

    ErlNifBinary src;
    if (!enif_inspect_binary(env, argv[1], &src)) {
        return enif_make_badarg(env);
    }

    const size_t dst_size = ZSTD_compressBound(src.size);
    if (ZSTD_isError(dst_size)) {
        return raise_zstd_exception(env, dst_size);
    }

    ErlNifBinary dst;
    enif_alloc_binary(dst_size, &dst);

    const size_t compressed_size =
        ZSTD_compress2(*cctx, dst.data, dst.size, src.data, src.size);
    if (ZSTD_isError(compressed_size)) {
        enif_release_binary(&dst);
        return raise_zstd_exception(env, compressed_size);
    }

    // remove trailing bytes
    enif_realloc_binary(&dst, compressed_size);

    return enif_make_binary(env, &dst);
}

static ERL_NIF_TERM cctx_get_cparam(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_CCtx** cctx;
    if (!enif_get_resource(env, argv[0], cctx_type, reinterpret_cast<void**>(&cctx))) {
        return enif_make_badarg(env);
    }

    unsigned int param;
    if (!enif_get_uint(env, argv[1], &param)) {
        return enif_make_badarg(env);
    }

    int value;
    const size_t result =
        ZSTD_CCtx_getParameter(*cctx, static_cast<ZSTD_cParameter>(param), &value);
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return enif_make_int(env, value);
}

static ERL_NIF_TERM cctx_set_cparam(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_CCtx** cctx;
    if (!enif_get_resource(env, argv[0], cctx_type, reinterpret_cast<void**>(&cctx))) {
        return enif_make_badarg(env);
    }

    unsigned int param;
    if (!enif_get_uint(env, argv[1], &param)) {
        return enif_make_badarg(env);
    }

    int value;
    if (!enif_get_int(env, argv[2], &value)) {
        return enif_make_badarg(env);
    }

    const size_t result =
        ZSTD_CCtx_setParameter(*cctx, static_cast<ZSTD_cParameter>(param), value);
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return argv[0];
}

static ERL_NIF_TERM cctx_reset(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_CCtx** cctx;
    if (!enif_get_resource(env, argv[0], cctx_type, reinterpret_cast<void**>(&cctx))) {
        return enif_make_badarg(env);
    }

    unsigned int reset_directive;
    if (!enif_get_uint(env, argv[1], &reset_directive)) {
        return enif_make_badarg(env);
    }

    const size_t result =
        ZSTD_CCtx_reset(*cctx, static_cast<ZSTD_ResetDirective>(reset_directive));
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return argv[0];
}

static ERL_NIF_TERM
cctx_set_pledged_src_size(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_CCtx** cctx;
    if (!enif_get_resource(env, argv[0], cctx_type, reinterpret_cast<void**>(&cctx))) {
        return enif_make_badarg(env);
    }

    unsigned long size;
    if (!enif_get_ulong(env, argv[1], &size)) {
        return enif_make_badarg(env);
    }

    const size_t result = ZSTD_CCtx_setPledgedSrcSize(*cctx, size);
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return argv[0];
}

static ERL_NIF_TERM
cctx_load_dictionary(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_CCtx** cctx;
    if (!enif_get_resource(env, argv[0], cctx_type, reinterpret_cast<void**>(&cctx))) {
        return enif_make_badarg(env);
    }

    ErlNifBinary src;
    if (!enif_inspect_binary(env, argv[1], &src)) {
        return enif_make_badarg(env);
    }

    const size_t result = ZSTD_CCtx_loadDictionary_advanced(
        *cctx,
        src.data,
        src.size,
        ZSTD_dlm_byCopy,
        ZSTD_dct_fullDict
    );
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return argv[0];
}

static ERL_NIF_TERM
cctx_compress_stream(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_CCtx** cctx;
    if (!enif_get_resource(env, argv[0], cctx_type, reinterpret_cast<void**>(&cctx))) {
        return enif_make_badarg(env);
    }

    ErlNifBinary src;
    if (!enif_inspect_binary(env, argv[1], &src)) {
        return enif_make_badarg(env);
    }

    unsigned int end_directive;
    if (!enif_get_uint(env, argv[2], &end_directive)) {
        return enif_make_badarg(env);
    }

    const size_t cstream_out_size = ZSTD_CStreamOutSize();
    ErlNifBinary dst;
    enif_alloc_binary(cstream_out_size, &dst);

    size_t src_offset = 0;
    size_t dst_offset = 0;
    do {
        if (dst.size - dst_offset < cstream_out_size) {
            enif_realloc_binary(&dst, dst.size * 2);
        }

        const size_t result = ZSTD_compressStream2_simpleArgs(
            *cctx,
            dst.data,
            dst.size,
            &dst_offset,
            src.data,
            src.size,
            &src_offset,
            static_cast<ZSTD_EndDirective>(end_directive)
        );
        if (ZSTD_isError(result)) {
            enif_release_binary(&dst);

            return raise_zstd_exception(env, result);
        }
    } while (src_offset < src.size);

    // remove trailing bytes
    enif_realloc_binary(&dst, dst_offset);

    return enif_make_binary(env, &dst);
}

/* Decompression context API */

static ERL_NIF_TERM dctx_new(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;
    (void) argv;

    ZSTD_DCtx** const dctx = alloc_resource<ZSTD_DCtx*>(dctx_type);
    *dctx = ZSTD_createDCtx_advanced(zstd_custom_mem);

    return make_resource(env, dctx);
}

static ERL_NIF_TERM dctx_decompress(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_DCtx** dctx;
    if (!enif_get_resource(env, argv[0], dctx_type, reinterpret_cast<void**>(&dctx))) {
        return enif_make_badarg(env);
    }

    ErlNifBinary src;
    if (!enif_inspect_binary(env, argv[1], &src)) {
        return enif_make_badarg(env);
    }

    // find output size
    const size_t decompressed_size = ZSTD_getFrameContentSize(src.data, src.size);
    switch (decompressed_size) {
    case ZSTD_CONTENTSIZE_UNKNOWN: {
        ERL_NIF_TERM const error_msg = enif_make_resource_binary(
            env,
            singleton_instance,
            CONTENT_SIZE_UNKNOWN_MSG,
            sizeof(CONTENT_SIZE_UNKNOWN_MSG)
        );
        return raise_exception(env, atom_exzstd_content_size_unknown, error_msg);
    }
    case ZSTD_CONTENTSIZE_ERROR: {
        ERL_NIF_TERM const error_msg = enif_make_resource_binary(
            env,
            singleton_instance,
            CONTENT_SIZE_ERROR_MSG,
            sizeof(CONTENT_SIZE_ERROR_MSG)
        );
        return raise_exception(env, atom_exzstd_content_size_error, error_msg);
    }
    default:
        break;
    }

    ErlNifBinary dst;
    enif_alloc_binary(decompressed_size, &dst);

    const size_t decompression_result =
        ZSTD_decompressDCtx(*dctx, dst.data, dst.size, src.data, src.size);
    if (ZSTD_isError(decompression_result)) {
        enif_release_binary(&dst);
        return raise_zstd_exception(env, decompression_result);
    }

    return enif_make_binary(env, &dst);
}

static ERL_NIF_TERM dctx_get_dparam(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_DCtx** dctx;
    if (!enif_get_resource(env, argv[0], dctx_type, reinterpret_cast<void**>(&dctx))) {
        return enif_make_badarg(env);
    }

    unsigned int param;
    if (!enif_get_uint(env, argv[1], &param)) {
        return enif_make_badarg(env);
    }

    int value;
    const size_t result =
        ZSTD_DCtx_getParameter(*dctx, static_cast<ZSTD_dParameter>(param), &value);
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return enif_make_int(env, value);
}

static ERL_NIF_TERM dctx_set_dparam(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_DCtx** dctx;
    if (!enif_get_resource(env, argv[0], dctx_type, reinterpret_cast<void**>(&dctx))) {
        return enif_make_badarg(env);
    }

    unsigned int param;
    if (!enif_get_uint(env, argv[1], &param)) {
        return enif_make_badarg(env);
    }

    int value;
    if (!enif_get_int(env, argv[2], &value)) {
        return enif_make_badarg(env);
    }

    const size_t result =
        ZSTD_DCtx_setParameter(*dctx, static_cast<ZSTD_dParameter>(param), value);
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return argv[0];
}

static ERL_NIF_TERM dctx_reset(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_DCtx** dctx;
    if (!enif_get_resource(env, argv[0], dctx_type, reinterpret_cast<void**>(&dctx))) {
        return enif_make_badarg(env);
    }

    unsigned int reset_directive;
    if (!enif_get_uint(env, argv[1], &reset_directive)) {
        return enif_make_badarg(env);
    }

    const size_t result =
        ZSTD_DCtx_reset(*dctx, static_cast<ZSTD_ResetDirective>(reset_directive));
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return argv[0];
}

static ERL_NIF_TERM
dctx_load_dictionary(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_DCtx** dctx;
    if (!enif_get_resource(env, argv[0], dctx_type, reinterpret_cast<void**>(&dctx))) {
        return enif_make_badarg(env);
    }

    ErlNifBinary src;
    if (!enif_inspect_binary(env, argv[1], &src)) {
        return enif_make_badarg(env);
    }

    const size_t result = ZSTD_DCtx_loadDictionary_advanced(
        *dctx,
        src.data,
        src.size,
        ZSTD_dlm_byCopy,
        ZSTD_dct_fullDict
    );
    if (ZSTD_isError(result)) {
        return raise_zstd_exception(env, result);
    }

    return argv[0];
}

static ERL_NIF_TERM
dctx_decompress_stream(ErlNifEnv* env, int argc, const ERL_NIF_TERM* argv) {
    (void) argc;

    ZSTD_DCtx** dctx;
    if (!enif_get_resource(env, argv[0], dctx_type, reinterpret_cast<void**>(&dctx))) {
        return enif_make_badarg(env);
    }

    ErlNifBinary src;
    if (!enif_inspect_binary(env, argv[1], &src)) {
        return enif_make_badarg(env);
    }

    const size_t dstream_out_size = ZSTD_DStreamOutSize();
    ErlNifBinary dst;
    enif_alloc_binary(dstream_out_size, &dst);

    size_t src_offset = 0;
    size_t dst_offset = 0;
    size_t result;
    do {
        if (dst.size - dst_offset < dstream_out_size) {
            enif_realloc_binary(&dst, dst.size * 2);
        }

        result = ZSTD_decompressStream_simpleArgs(
            *dctx,
            dst.data,
            dst.size,
            &dst_offset,
            src.data,
            src.size,
            &src_offset
        );
        if (ZSTD_isError(result)) {
            enif_release_binary(&dst);

            return raise_zstd_exception(env, result);
        }
    } while (src_offset < src.size);

    // remove trailing bytes
    enif_realloc_binary(&dst, dst_offset);

    return enif_make_tuple2(
        env,
        enif_make_ulong(env, result),
        enif_make_binary(env, &dst)
    );
}

#define EXZSTD_NIF(NAME, ARITY, FLAGS) \
    { #NAME, ARITY, NAME, FLAGS }

EXZSTD_EXPORT ErlNifFunc zstd_exports[] = {
    // Simple API

    EXZSTD_NIF(get_cparam_bounds, 1, 0),
    EXZSTD_NIF(get_dparam_bounds, 1, 0),
    EXZSTD_NIF(content_size_unknown, 0, 0),
    EXZSTD_NIF(get_frame_content_size, 1, 0),

    // Compression context API

    EXZSTD_NIF(cctx_new, 0, 0),
    {"cctx_compress!", 2, cctx_compress, ERL_DIRTY_JOB_CPU_BOUND},
    EXZSTD_NIF(cctx_get_cparam, 2, 0),
    EXZSTD_NIF(cctx_set_cparam, 3, 0),
    EXZSTD_NIF(cctx_reset, 2, 0),
    EXZSTD_NIF(cctx_set_pledged_src_size, 2, 0),
    EXZSTD_NIF(cctx_load_dictionary, 2, 0),
    {"cctx_compress_stream!", 3, cctx_compress_stream, ERL_DIRTY_JOB_CPU_BOUND},

    // Decompression context API

    EXZSTD_NIF(dctx_new, 0, 0),
    {"dctx_decompress!", 2, dctx_decompress, ERL_DIRTY_JOB_CPU_BOUND},
    EXZSTD_NIF(dctx_get_dparam, 2, 0),
    EXZSTD_NIF(dctx_set_dparam, 3, 0),
    EXZSTD_NIF(dctx_reset, 2, 0),
    EXZSTD_NIF(dctx_load_dictionary, 2, 0),
    {"dctx_decompress_stream!", 2, dctx_decompress_stream, ERL_DIRTY_JOB_CPU_BOUND},
};
// clang-format on

} // namespace exzstd

ERL_NIF_INIT(
    Elixir.ExZstd.Nif,
    exzstd::zstd_exports,
    exzstd::on_load,
    exzstd::on_reload,
    exzstd::on_upgrade,
    exzstd::on_unload
)
