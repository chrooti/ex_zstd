defmodule ExZstd.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/chrooti/ex_zstd"

  def project do
    [
      app: :ex_zstd,
      deps: deps(),
      elixir: "~> 1.7",
      version: "0.5.3",
      package: package(),
      compilers: compilers(),
      description: description(),
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      dialyzer: dialyzer(),

      # Docs
      source_url: @source_url,
      docs: [
        source_ref: "master"
      ]
    ]
  end

  defp deps,
    do: [
      {:ex_doc, ">= 0.0.0", only: [:dev], runtime: false},
      {:credo, "~> 1.6", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev, :test], runtime: false}
    ]

  defp package,
    do: [
      name: "ex_zstd_reloaded",
      files: [
        "c_src",
        "lib",
        "vendor/zstd.tar.gz",
        ".formatter.exs",
        "mix.exs",
        "LICENSE",
        "Makefile",
        "README.md"
      ],
      links: %{"Gitlab" => @source_url},
      licenses: ["MIT"]
    ]

  defp compilers,
    do: [
      :ex_zstd,
      :elixir,
      :app
    ]

  defp description,
    do: "Elixir bindings to the Zstandard library"

  defp aliases,
    do: [
      check: [
        "compile --warnings-as-errors",
        "format --check-formatted",
        "credo --strict",
        "dialyzer"
      ],
      "deps.update.all": ["deps.unlock --all", "deps.update --all"],
      "deps.clean.unused": "deps.clean --unlock --unused"
    ]

  defp dialyzer(),
    do: [
      ignore_warnings: ".dialyzer_ignore.exs",
      list_unused_filters: true,
      plt_add_deps: :app_tree,
      flags: [
        # https://www.erlang.org/doc/man/dialyzer.html#warning_options
        :error_handling,
        :underspecs,
        :unknown,
        :extra_return,
        :missing_return
      ]
    ]
end

defmodule Mix.Tasks.Compile.ExZstd do
  @moduledoc """
  Compiles the Nif in the ExZstd library.
  """

  use Mix.Task.Compiler

  @impl true
  def run(_args) do
    cpus = :erlang.system_info(:logical_processors_available)

    {_, 0} =
      System.cmd("make", [
        "--jobs=#{cpus}"
      ])

    [Mix.Project.build_path(), "lib/ex_zstd/priv"]
    |> Path.join()
    |> tap(&File.mkdir_p!/1)
    |> then(&File.cp!("priv/c_out/ex_zstd_nif.so", "#{&1}/ex_zstd_nif.so"))

    :ok
  end

  @impl true
  def clean do
    {_, 0} = System.cmd("make", ["really-clean"])
  end
end
