# ExZstd

Fork (and complete rewrite) of the unmantained [ExZstd](https://github.com/chenzhuoyu/elixir-zstd).

## Installation

The package can be installed by adding `ex_zstd_reloaded` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ex_zstd, "~> 0.5.3", hex: :ex_zstd_reloaded}
  ]
end
```

The docs can be found at [https://hexdocs.pm/ex_zstd_reloaded](https://hexdocs.pm/ex_zstd_reloaded).

## Development environment

```bash
docker build . --force-rm --tag ex_zstd -f ci/debian.Containerfile
PWD="$(pwd)"; docker run -it --rm --volume "$PWD":"$PWD" --workdir "$PWD" ex_zstd:latest bash
```

## Note on the changelog

A changelog file has been introduced since v 0.5.2 and conventional commits.
Since the versions before 0.5.0 are not really considered suitable for production and the history was a mess anyway, I decided to start with a clean slate.
Commits before version 0.5.2 (excluding the original project ones) only show the version, for historical purposes.
