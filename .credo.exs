%{
  configs: [
    %{
      name: "default",
      strict: true,
      checks: %{
        enabled: [
          {Credo.Check.Consistency.ExceptionNames, false},
        ]
      }
    }
  ]
}
