defmodule ExZstd.ContentSizeError do
  defexception [:message]
end
