defmodule ExZstd.Cparam.Strategy do
  @moduledoc """
  Helpers to convert the values of the `strategy` cparam into numbers. Consult zstd.h to know more.
  """

  def fast, do: 1
  def dfast, do: 2
  def greedy, do: 3
  def lazy, do: 4
  def lazy2, do: 5
  def btlazy2, do: 6
  def btopt, do: 7
  def btultra, do: 8
  def btultra2, do: 9
end
