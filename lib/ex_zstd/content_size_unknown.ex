defmodule ExZstd.ContentSizeUnknown do
  defexception [:message]
end
