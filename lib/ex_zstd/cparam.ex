defmodule ExZstd.Cparam do
  @moduledoc """
  Helpers to convert cparam names into numbers. Consult zstd.h to know more.
  """

  @type t() :: pos_integer()

  def compression_level, do: 100
  def window_log, do: 101
  def hash_log, do: 102
  def chain_log, do: 103
  def search_log, do: 104
  def min_match, do: 105
  def target_length, do: 106
  def strategy, do: 107
  def enable_long_distance_matching, do: 160
  def ldm_hash_log, do: 161
  def ldm_min_match, do: 162
  def ldm_bucket_size_log, do: 163
  def ldm_hash_rate_log, do: 164
  def content_size_flag, do: 200
  def checksum_flag, do: 201
  def dict_id_flag, do: 202
  def nb_workers, do: 400
  def job_size, do: 401
  def overlap_log, do: 402
end
