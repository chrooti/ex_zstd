defmodule ExZstd.Dparam do
  @moduledoc """
  Helpers to convert dparam names into numbers. Consult zstd.h to know more.
  """

  @type t() :: pos_integer()

  def window_log_max, do: 100
end
