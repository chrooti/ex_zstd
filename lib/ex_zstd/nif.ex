defmodule ExZstd.Nif do
  @moduledoc false

  alias ExZstd.CCtx

  # TODO: warning: index.html redirects to ex_zstd.html, which does not exist

  @on_load :init

  def init do
    :ok =
      :ex_zstd
      |> :code.priv_dir()
      |> Path.join("ex_zstd_nif")
      |> String.to_charlist()
      |> :erlang.load_nif(0)
  end

  # Simple API

  def get_cparam_bounds(_param),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def get_dparam_bounds(_dparam),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def content_size_unknown,
    do: :erlang.nif_error(:nif_library_not_loaded)

  def get_frame_content_size(_data),
    do: :erlang.nif_error(:nif_library_not_loaded)

  # Compression context API

  def cctx_new,
    do: :erlang.nif_error(:nif_library_not_loaded)

  def cctx_compress!(_cctx, _data),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def cctx_get_cparam(_cctx, _cparam),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def cctx_set_cparam(_cctx, _cparam, _value),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def cctx_reset(_cctx, _reset_directive \\ ExZstd.reset_session_and_parameters()),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def cctx_set_pledged_src_size(_cctx, _size), do: :erlang.nif_error(:nif_library_not_loaded)

  def cctx_load_dictionary(_cctx, _dictionary),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def cctx_compress_stream!(_cstream, _data, _end_directive \\ CCtx.stream_end()),
    do: :erlang.nif_error(:nif_library_not_loaded)

  # Decompression context API

  def dctx_new,
    do: :erlang.nif_error(:nif_library_not_loaded)

  def dctx_decompress!(_dctx, _data),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def dctx_get_dparam(_dctx, _dparam),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def dctx_set_dparam(_dctx, _dparam, _value),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def dctx_reset(_dctx, _reset_directive \\ ExZstd.reset_session_and_parameters()),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def dctx_load_dictionary(_dctx, _dictionary),
    do: :erlang.nif_error(:nif_library_not_loaded)

  def dctx_decompress_stream!(_dctx, _data),
    do: :erlang.nif_error(:nif_library_not_loaded)
end
