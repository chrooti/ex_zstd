0.5.3
- build: typo in Makefile COMMON_CFLAGS -> COMMON_FLAGS (thanks <ozyair85@gmail.com>)

0.5.2
- build: use prod env, work without llvm-config
- docs: introduce changelog
- build: revert usage of MIX_ENV=prod
